package com.demo.springboot.rest;

public class CheckId {

    static public boolean checkPesel(String id) {
        if (id.length() != 11) {
            return false;
        }

        int[] numberControl = {1,3,7,9,1,3,7,9,1,3};
        char[] pesel = id.toCharArray();
        int sum = 0;
        for (int i = 0; i < numberControl.length; i++) {
            sum += numberControl[i] * Character.getNumericValue(pesel[i]);
        }
        return sum % Character.getNumericValue(pesel[10]) == 0;
    }
}
